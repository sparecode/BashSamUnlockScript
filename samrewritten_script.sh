#!/usr/bin/bash"

#GLOBAL VARIABLES
_time=2	#sleep time between game checks =!REQUESTED!=
RED="\e[31m"
CYAN="\e[36m"
YELLOW="\e[33m"
ENDCOLOR="\e[0m"


#ASSEMBLE APP LIST
app_list() {
	local IFS=$'\n'
	local _app_list=()
	mapfile _app_list < <( samrewritten --apps )
	_app_list=("${_app_list[@]:1}")
	printf "%s\n" ${_app_list[@]}
}
#app_list

#EXPORT APP LIST
exp_list() {
	local IFS=$'\n'
	local _app_list=()
	local _path=app_list.txt
	mapfile _app_list < <( app_list )
	echo -e "${YELLOW}${#_app_list[@]} item discovered${ENDCOLOR}"
	if [[ -n $1 ]]
	then
		_path=$1
	fi
	echo -e "${CYAN}Exporting to '$_path'...${ENDCOLOR}"
	printf "%s" "${_app_list[@]}" > "$_path"
}
#exp_list
#exp_list "blk_list.txt"

#PARSE LISTS
prs_list(){
	local IFS=$'\n'
	local _pre_list=()
	local _pst_list=()
	mapfile _pre_list < <( $1 )
	mapfile _pst_list < <( for a in ${_pre_list[@]}; do echo -e "${a[@]%% *}" | tr -s '[:blank:]' ' '; done )
	printf "%s\n" ${_pst_list[@]}
}
#prs_list app_list

#LOAD BLACKLIST

blk_list(){
	local IFS=$'\n'
	local _blk_list=()
	local _path="blk_list.txt"
	if [[ -n ${1} ]]
	then
		_path=$1
	fi
	mapfile _blk_list < ${_path}
	printf "%s" "${_blk_list[@]}"
}
#blk_list
#blk_list "blacklist.txt"

#CHECK ACHIEVMENTS
chk_achiev(){
	IFS=$'\n'
	local _id=$1
	local _achiev=()
	mapfile _achiev < <( samrewritten $1 --ls --filter-protected no --filter-achieved no --nostats | tr '[:upper:]' '[:lower:]')
	_achiev=("${_achiev[@]:2}")
	mapfile _achiev < <( for a in ${_achiev[@]}; do echo -e ${a[@]# }| tr -s '[:blank:]' ' '; done )
	_achiev=$( for b in ${_achiev[@]}; do echo "${b[@]%% *}" | tr -s '[:space:]' ','; done )
	_achiev=${_achiev%,*}
	if [[ -n ${_achiev} ]]
	then
		echo -e "${CYAN}Unlocking achievments for $1...${ENDCOLOR}"
		$(samrewritten $1 --unlock $_achiev)
	else
		echo -e "${RED}No achievments to unlock for $1!${ENDCOLOR}"
	fi
}
#chk_achiev 1468260

#CHECK BLACKLIST
chk_list(){
	local _app_list=()
	mapfile _app_list < <(prs_list app_list)
	for a in ${_app_list[@]}
	do
		if [[ $(echo $(blk_list $1) | grep "${a}") ]]
		then
			echo -e "${YELLOW}Blacklist contains '$a'!${ENDCOLOR}"
		else
			echo -e "${CYAN}Trying to unlock achievements '$a'...${ENDCOLOR}"
			chk_achiev $a
			sleep $_time
		fi
	done
}
#chk_list "blklist.txt"

#ARGUEMENT/FLAG HANDLING
_blk_path=
_exp_path=
while getopts o:b: flag
do
	case "${flag}" in
		b) _blk_path=${OPTARG};;
		o) _exp_path=${OPTARG};;

	esac
done
if [[ -n ${_blk_path} ]]
then
	echo "_blk_path: $_blk_path"
	chk_list ${_blk_path}
fi
if [[ -n ${_exp_path} ]]
then
echo "_exp_path: $_exp_path"
	exp_list $_exp_path
fi
