# Steam Achievement Unlock Script, Linux

## About

calls samrewritten to manage steam achievements in bulk

## Requirement

* must have linux steam installed
* must build [samrewritten](https://github.com/PaulCombal/SamRewritten), not release

## Use

the script can be used for two things: to dump a list of all owned apps to a file and unlock all achievements from apps not specified on a chosen blacklist.

to dump a list of own apps from the currently logged in user:
`samrewritten_script -o "FILEPATH.txt"`

to unlock all achievements not on a blacklist of app ids (line separated):
`samrewritten_script -b "FILEPATH.txt"`

the commands can be combined to get a list of owned apps, unlock achievements and then export the list for quick reuse:
`samrewritten_script -b "FILEPATH.txt" -o "FILEPATH.txt"`
